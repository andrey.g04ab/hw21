from flask import Flask, jsonify
import requests
url='http://hw_21_api1_1:5001/'

app = Flask(__name__)

@app.route("/")
def home():
    try:
        res=requests.get(url)
        if res.status_code==200:
            return f'1>{res.text}'
        return '1>'+'error'    
    except:  
        return '1>'+'error'
           


if __name__ == "__main__":
     app.run(host='0.0.0.0',port=5000)  