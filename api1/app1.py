from flask import Flask, jsonify
import requests
url='http://hw_21_api2_1:5002/'

app1 = Flask(__name__)


@app1.route("/")
def home():
    try:
        res=requests.get(url)
        if res.status_code==200:
            return f'2>{res.text}'
        return '2>'+'error'    
    except:
        return '2>'+'error'    


if __name__ == "__main__":
     app1.run(host='0.0.0.0',port=5001)